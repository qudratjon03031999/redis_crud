package com.koinot.koinotredis.repository;

import com.koinot.koinotredis.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, String> {

	List<User> findByFirstNameAndLastName(String firstName, String lastName);

	List<User> findAllBy();

}
