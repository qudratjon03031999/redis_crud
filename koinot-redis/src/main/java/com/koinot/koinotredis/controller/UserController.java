package com.koinot.koinotredis.controller;

import com.koinot.koinotredis.domain.User;
import com.koinot.koinotredis.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

	@Autowired
	private UserRepository userRepository;

	@RequestMapping("/saveUser")
	public List<User> saveUser() {
		//SSD java backend developer

		List<User> users=new ArrayList<>();

		User user = new User("1","Qudratjon","Qobil o'g'li","Komilov",
				"DEVELOPER");
		users.add(userRepository.save(user));

		user = new User("2","Assomiddin","Sirojiddin o'g'li","Raxmidinov",
				"DEVELOPER");
		users.add(userRepository.save(user));

		user = new User("3","Madiyor","Baxrom  o'g'li","Qahhorov",
				"DEVELOPER");
		users.add(userRepository.save(user));

		user = new User("4","Baxtiyor","Rustammuvich","Ro'zimatov",
				"Java Head");
		users.add(userRepository.save(user));

		user = new User("5","Xurshida","","Sadulayeva",
				"DEVELOPER");
		users.add(userRepository.save(user));

		user = new User("6","Sarvar","Jamshid o'g'li","Xo'jayev",
				"DEVELOPER");
		users.add(userRepository.save(user));
		return users;
	}

	@GetMapping("/getUserFirstLast")
	public List<User> getUser(@RequestParam String firstName, @RequestParam String lastName) {
		return userRepository.findByFirstNameAndLastName(firstName, lastName);
	}

	@GetMapping("/all")
	public List<User> getAllUser() {
		return userRepository.findAllBy();
	}

	@GetMapping("/getUserId")
	public Optional<User> getUser(@RequestParam String id) {
		return userRepository.findById(id);
	}

	@PutMapping("/putUser")
	public String putUser(@RequestBody User user) {
		userRepository.save(user);
		return "Done";
	}

	@DeleteMapping("/delete")
	public String deleteUser(@RequestParam String id) {
		userRepository.deleteById(id);
		return "Done";
	}
}
