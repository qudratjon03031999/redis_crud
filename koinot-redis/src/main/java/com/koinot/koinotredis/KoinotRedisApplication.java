package com.koinot.koinotredis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KoinotRedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(KoinotRedisApplication.class, args);
    }

}
